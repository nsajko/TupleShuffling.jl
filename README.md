# TupleShuffling

[![PkgEval](https://JuliaCI.github.io/NanosoldierReports/pkgeval_badges/T/TupleShuffling.svg)](https://JuliaCI.github.io/NanosoldierReports/pkgeval_badges/T/TupleShuffling.html)
[![Aqua](https://raw.githubusercontent.com/JuliaTesting/Aqua.jl/master/badge.svg)](https://github.com/JuliaTesting/Aqua.jl)

Static shuffling and static random permutations: shuffling of tuples and generating random permutation tuples.

## Performance

Some benchmarks show that the sorting method is faster than Fisher-Yates:

```julia-repl
julia> using TupleShuffling, BenchmarkTools, Random

julia> const TS = TupleShuffling
TupleShuffling

julia> const prg = Xoshiro()
Xoshiro(0xaeae398d1787470e, 0x2b1dfb052074e02a, 0x670e6c522813055b, 0xb8ca12a2747a448b, 0x40d6caa8709ce220)

julia> shuf_generic(tup, alg) = TS.shuffle(prg, tup, alg)
shuf_generic (generic function with 1 method)

julia> shuf_sort(tup) = shuf_generic(tup, TS.option_sort)
shuf_sort (generic function with 1 method)

julia> shuf_fiya(tup) = shuf_generic(tup, TS.option_fisher_yates)
shuf_fiya (generic function with 1 method)

julia> function f!(v, s::S) where {S}
         for i ∈ 0:(length(v) - 1)
           v[begin + i] = s(v[begin + i])
         end
       end
f! (generic function with 1 method)

julia> vec(::Val{n}, len) where {n} = [ntuple((_ -> rand()), Val(n)) for _ ∈ 1:len]
vec (generic function with 1 method)

julia> @btime f!(v, shuf_fiya) setup=(v=vec(Val(4),10_000);)
  297.494 μs (0 allocations: 0 bytes)

julia> @btime f!(v, shuf_sort) setup=(v=vec(Val(4),10_000);)
  178.994 μs (0 allocations: 0 bytes)

julia> @btime f!(v, shuf_fiya) setup=(v=vec(Val(5),10_000);)
  433.728 μs (0 allocations: 0 bytes)

julia> @btime f!(v, shuf_sort) setup=(v=vec(Val(5),10_000);)
  295.530 μs (0 allocations: 0 bytes)

julia> @btime f!(v, shuf_fiya) setup=(v=vec(Val(6),10_000);)
  540.567 μs (0 allocations: 0 bytes)

julia> @btime f!(v, shuf_sort) setup=(v=vec(Val(6),10_000);)
  352.276 μs (0 allocations: 0 bytes)

julia> @btime f!(v, shuf_fiya) setup=(v=vec(Val(7),10_000);)
  655.022 μs (0 allocations: 0 bytes)

julia> @btime f!(v, shuf_sort) setup=(v=vec(Val(7),10_000);)
  488.831 μs (0 allocations: 0 bytes)

julia> versioninfo()
Julia Version 1.11.0-DEV.761
Commit 9c581cd719f (2023-10-28 11:49 UTC)
Build Info:
  Official https://julialang.org/ release
Platform Info:
  OS: Linux (x86_64-linux-gnu)
  CPU: 8 × AMD Ryzen 3 5300U with Radeon Graphics
  WORD_SIZE: 64
  LLVM: libLLVM-15.0.7 (ORCJIT, znver2)
  Threads: 11 on 8 virtual cores
Environment:
  JULIA_NUM_PRECOMPILE_TASKS = 3
  JULIA_PKG_PRECOMPILE_AUTO = 0
  JULIA_EDITOR = code
  JULIA_NUM_THREADS = 8
```
