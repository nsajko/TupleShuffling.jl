# Copyright © 2023 Neven Sajko. All rights reserved.

module PermutedTuple

import ..TupleUtils

const NT = TupleUtils.NT
const r = TupleUtils.rough_type_of

function permute(t::NT{n}, p::NTuple{n,Integer}) where {n}
  f = let t = t, p = p
    i -> t[p[i]]
  end
  ntuple(f, Val(n))::r(t)
end

end
