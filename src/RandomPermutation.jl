# Copyright © 2023 Neven Sajko. All rights reserved.

module RandomPermutation

import ..Shuffle

function random_permutation(prg::P, ::Val{n}, algo::A, b::T = 1) where {P, n, A, T}
  f = let b = b
    i -> T(i + b - 1)::T
  end
  id = ntuple(f, Val(n))::NTuple{n,T}
  Shuffle.shuffle(prg, id, algo)::NTuple{n,T}
end

end
