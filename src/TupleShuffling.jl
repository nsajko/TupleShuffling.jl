# Copyright © 2023 Neven Sajko. All rights reserved.

module TupleShuffling

include("Shuffle.jl")
include("TupleUtils.jl")
include("TupleWithoutElementAtIndex.jl")
include("PermutedTuple.jl")
include("TupleShufflingFisherYates.jl")
include("TupleShufflingSort.jl")
include("RandomPermutation.jl")

"""
Selects the Fisher-Yates algorithm when passed to [`shuffle`](@ref) or
[`random_permutation`](@ref).
"""
const option_fisher_yates = TupleShufflingFisherYates.Algo()

"""
Selects the sorting algorithm when passed to [`shuffle`](@ref) or
[`random_permutation`](@ref).
"""
const option_sort = TupleShufflingSort.Algo()

"""
Call like `shuffle(prg, tup, alg)`.

`prg` is a pseudorandom number generator, e.g., one of the RNGs from Julia's `Random`
standard library.

`tup` is a tuple that will be shuffled.

`alg` is the option for selecting the algorithm used.
"""
const shuffle = Shuffle.shuffle

"""
Call like `random_permutation(prg, Val(n), alg, b = 1)`.

`T` will be the element type of the returned permutation.

`prg` is a pseudorandom number generator, e.g., one of the RNGs from Julia's `Random`
standard library.

`n` will be the length of the returned permutation.

`alg` is the option for selecting the algorithm used.

`b` will be the indexing base of the returned permutation. It also determines the
element type of the permutation.
"""
const random_permutation = RandomPermutation.random_permutation

end
