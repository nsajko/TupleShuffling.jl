# Copyright © 2023 Neven Sajko. All rights reserved.

module TupleShufflingFisherYates

import TupleSorting, ..Shuffle, ..TupleUtils, ..TupleWithoutElementAtIndex

const without_elem = TupleWithoutElementAtIndex.without_elem_at_index
const r = TupleUtils.rough_type_of

shuf(::Type, ::Any, ::Tuple{}) = ()

function shuf(::Type{T}, prg::P, t::Tuple{Any,Vararg{Any,n}}) where {T, P, n}
  i = rand(prg, Base.OneTo(T(n + 1)::T))::T
  todo = without_elem(t, i)
  TupleSorting.cat(t[i], shuf(T, prg, todo))::r(t)
end

struct Algo end

Shuffle.shuffle(prg::P, t::T, ::Algo) where {P, T<:Tuple} =
  shuf(UInt8, prg, t)::r(t)

end
