# Copyright © 2023 Neven Sajko. All rights reserved.

module TupleShufflingSort

import TupleSorting, ..Shuffle, ..TupleUtils

const r = TupleUtils.rough_type_of
const t_map = TupleUtils.tuple_map
const TS = TupleSorting
const sorted = TS.sorted
const SLS = TS.StaticLengthSort

function shuf(::Type{T}, prg::P, t::Tu, ::Val{m}) where {T, P, Tu<:Tuple, m}
  rnd = let prg = prg
    e -> (rand(prg, T)::T, e)
  end
  s = t_map(rnd, t)
  u = sorted(s, SLS{m}(), Base.Order.By(first))
  t_map(last, u)::r(t)
end

struct Algo end

Shuffle.shuffle(prg::P, t::T, ::Algo) where {P, T<:Tuple} =
  shuf(UInt64, prg, t, Val(12))::r(t)

end
