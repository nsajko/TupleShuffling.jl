# Copyright © 2023 Neven Sajko. All rights reserved.

module TupleUtils

const NT = NTuple{n,Any} where {n}

rough_type_of(::T) where {n, T<:NT{n}} = NTuple{n,eltype(T)}

# `map` optimized for tuples. Necessary for Julia v1.6.
function tuple_map(f::F, t::NTuple{n,Any}) where {F, n}
  g = let t = t
    i -> t[i]
  end
  ntuple(f ∘ g, Val(n))
end

end
