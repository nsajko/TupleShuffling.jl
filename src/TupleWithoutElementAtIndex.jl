# Copyright © 2023 Neven Sajko. All rights reserved.

module TupleWithoutElementAtIndex

err_bounds(t::Tuple, i::Integer) = throw(BoundsError(t, i))

without_elem_at_index(t::Tuple{}, i::Integer) = err_bounds(t, i)

axis(c) = (only ∘ axes)(c)

function without_elem_at_index(t::Tuple{Any,Vararg{Any,n}}, i::Integer) where {n}
  (i ∈ axis(t)) || err_bounds(t, i)
  f = let t = t, i = i
    j -> (j < i) ? t[j] : t[j + 1]
  end
  ntuple(f, Val(n))::NTuple{n,eltype(t)}
end

end
