import TupleShuffling, TupleSorting, Aqua, Random
using Test: @testset, @test, @inferred

const TS = TupleShuffling
const TSo = TupleSorting

const max_supported_len = 30
const max_supported_len_val = Val(max_supported_len)

const PRG = Random.MersenneTwister
big_tuple(::Type{T} = Float64, ::Val{n} = max_supported_len_val) where {T, n} =
  ntuple((_ -> rand(T)), Val(n))
const big_tuple_const = big_tuple()
const opt_sort = TS.option_sort
const opt_fiya = TS.option_fisher_yates
const opts = (opt_sort, opt_fiya)

const shuf = let prg = PRG()
  (tup, alg) -> TS.shuffle(prg, tup, alg)
end

const randperm = let prg = PRG()
  function(::Val{n}, alg, b) where {n}
    TS.random_permutation(prg, Val(n), alg, b)
  end
end

const srp = sort ∘ collect ∘ randperm

@testset "TupleShuffling.jl" begin
  @testset "Code quality (Aqua.jl)" begin
    Aqua.test_all(TS)
  end

  @testset "shuffling" begin
    @testset "heterogeneous" for o ∈ opts
      @test shuf((Val(1), Val(2), Val(3)), o) isa NTuple{3,Val}
      @test shuf((Val(1), nothing, Val(3)), o) isa NTuple{3,Union{Val,Nothing}}
    end

    @testset "sorted" begin
      s = TSo.sorted(big_tuple_const)
      @test s == TSo.sorted(shuf(big_tuple_const, opt_fiya))
      @test s == TSo.sorted(shuf(big_tuple_const, opt_sort))
    end

    @testset "alloc" begin
      @test iszero(@allocated shuf(big_tuple_const, opt_fiya))
      @test iszero(@allocated shuf(big_tuple_const, opt_sort))
    end
  end

  @testset "random permutations" begin
    bs_int = -2:2
    bs = (bs_int..., map(Int8, bs_int)...)

    @testset "isperm" begin
      @test isperm(randperm(max_supported_len_val, opt_fiya, one(Int32)))
      @test isperm(randperm(max_supported_len_val, opt_fiya, one(Int64)))
      @test isperm(randperm(max_supported_len_val, opt_sort, one(Int32)))
      @test isperm(randperm(max_supported_len_val, opt_sort, one(Int64)))
    end

    @testset "alloc" begin
      @test iszero(@allocated randperm(max_supported_len_val, opt_fiya, one(Int32)))
      @test iszero(@allocated randperm(max_supported_len_val, opt_fiya, one(Int64)))
      @test iszero(@allocated randperm(max_supported_len_val, opt_sort, one(Int32)))
      @test iszero(@allocated randperm(max_supported_len_val, opt_sort, one(Int64)))
    end

    @testset "empty" for o ∈ opts, b ∈ bs
      @test isempty(@inferred randperm(Val(0), o, b))
      @test (isempty ∘ srp)(Val(0), o, b)
    end

    @testset "single element" for o ∈ opts, b ∈ bs
      @test b == only(@inferred randperm(Val(1), o, b))
      @test (==(b) ∘ only ∘ srp)(Val(1), o, b)
    end

    @testset "many elements" for o ∈ opts, b ∈ bs, len ∈ 2:max_supported_len
      @test length(@inferred randperm(Val(len), o, b)) == len
      @test (==((b):(b + len - 1)) ∘ collect ∘ srp)(Val(len), o, b)
    end
  end
end
